$(document).ready(function(){
    var oTop = $("#skill").offset().top - window.innerHeight;
    var start = 0;
    $(window).scroll(function(){
        var pTop = $('body').scrollTop();
        if( pTop > oTop ){
            if (start == 0){
                $('.second.circle').circleProgress({
                    value: 0.6,
                    fill: { color: '#2ac8d5'},
                    lineCap: 'round',
                });
                start = 1;
            }
        }
    });
});
