<html>
    <head>
        <meta charset="UTF-8">
        <title>Ambient Arts | Home</title>
        <!--meta SEO-->
        <meta name="robots" content="index, follow">
        <meta name="og:description" content="">
        <meta name="Keywords" content="">
        <!--Import Google Icon Font-->
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

        {{HTML::style("public/packages/css/bootstrap.min.css")}}
        {{HTML::style("public/packages/css/style.css")}}

        {{HTML::script("public/packages/js/jquery-2.2.2.min.js")}}
        {{HTML::script("public/packages/js/jquery-circle-progress-1.1.3/dist/circle-progress.js")}}
        {{HTML::script("public/packages/js/script.js")}}
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, user-scalable=no">
    </head>
    <body>
        <section id='header'>
            <header>
                <div class='container'>
                    <div class='row'>
                        <div class='col-lg-4 logo'>
                            <a href='#'><h1>Ambient Arts</h1></a>
                        </div>
                        <div class='col-lg-8'>
                            <nav id='nav-principal'>
                                <ul class='nav navbar-nav'>
                                    <li>
                                        <a href='#'>Home</a>
                                    </li>
                                    <li>
                                        <a href='#'>Quem Somos</a>
                                    </li>
                                    <li>
                                        <a href='#'>O que fazemos</a>
                                    </li>
                                    <li>
                                        <a href='#'>Como funciona</a>
                                    </li>
                                    <li>
                                        <a href='#'>Preços</a>
                                    </li>
                                    <li>
                                        <a href='#'>Contato</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </header>
        </section>
        <section id='banner'>
            <div class='ban-text'>
                <h3 class='ban1'><span>VÁ MAIS LONGE</span></h3>
                <h3 class='ban2'><span>SOMOS INOVADORES E ANTENADOS</span></h3>
            </div>
        </section>
        <section id='empresa'>
            <div class='container'>
                <div class='row'>
                    <h2>A EMPRESA</h2>
                    <h3>MAIS DE 10 ANOS DE EXPERIÊNCIA NO UNIVERSO WEB E GRÁFICO</h3>
                    <div class='col-lg-4 col-md-4 col-sm-12 col-xs-12 col-text'>
                        <h4 class='hblue'>QUEM</h4>
                        <h4 class='hblack'>SOMOS</h4>
                        <p>
                            Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.
                        </p>
                    </div>
                    <div class='col-lg-4 col-md-4 col-sm-12 col-xs-12 col-text'>
                        <h4 class='hblue'>NOSSA</h4>
                        <h4 class='hblack'>ABORDAGEM</h4>
                        <p>
                            Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.
                        </p>
                    </div>
                    <div class='col-lg-4 col-md-4 col-sm-12 col-xs-12 col-text'>
                        <h4 class='hblue'>PORQUE</h4>
                        <h4 class='hblack'>NOS ESCOLHER</h4>
                        <p>
                            Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section id='fazemos'>
            <div class='container'>
                <div class='row'>

                    <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12  col-text'>
                        <div class='col-lg-2 image'>
                            <img src='public/packages/img/web.png' class='img-responsive' />
                        </div>
                        <div class='col-lg-10'>
                            <h4>DESENVOLVIMENTO WEB</h4>
                            <p>
                                Proin eget tortor risus. Mauris blandit
                                aliquet elit, eget tincidunt nibh pulvinar
                            </p>
                        </div>
                    </div>
                    <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12  col-text'>
                        <div class='col-lg-2 image'>
                            <img src='public/packages/img/responsivo.png' class='img-responsive' />
                        </div>
                        <div class='col-lg-10'>
                            <h4>SITES RESPONSIVOS</h4>
                            <p>
                                Proin eget tortor risus. Mauris blandit
                                aliquet elit, eget tincidunt nibh pulvinar
                            </p>
                        </div>
                    </div>
                    <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12  col-text'>
                        <div class='col-lg-2 image'>
                            <img src='public/packages/img/midias.png' class='img-responsive' />
                        </div>
                        <div class='col-lg-10'>
                            <h4>MÍDIAS SOCIAIS</h4>
                            <p>
                                Proin eget tortor risus. Mauris blandit
                                aliquet elit, eget tincidunt nibh pulvinar
                            </p>
                        </div>
                    </div>
                    <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12  col-text'>
                        <div class='col-lg-2 image'>
                            <img src='public/packages/img/seo.png' class='img-responsive' />
                        </div>
                        <div class='col-lg-10'>
                            <h4>OTIMIZAÇÃO SEO</h4>
                            <p>
                                Proin eget tortor risus. Mauris blandit
                                aliquet elit, eget tincidunt nibh pulvinar
                            </p>
                        </div>
                    </div>
                    <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12  col-text'>
                        <div class='col-lg-2 image'>
                            <img src='public/packages/img/visual.png' class='img-responsive' />
                        </div>
                        <div class='col-lg-10'>
                            <h4>IDENTIDADE VISUAL</h4>
                            <p>
                                Proin eget tortor risus. Mauris blandit
                                aliquet elit, eget tincidunt nibh pulvinar
                            </p>
                        </div>
                    </div>
                    <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12  col-text'>
                        <div class='col-lg-2 image'>
                            <img src='public/packages/img/mail.png' class='img-responsive' />
                        </div>
                        <div class='col-lg-10'>
                            <h4>E-MAIL MARKETING</h4>
                            <p>
                                Proin eget tortor risus. Mauris blandit
                                aliquet elit, eget tincidunt nibh pulvinar
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id='price'>
            <div class='container'>
                <div class='row'>
                    <h2>NOSSOS PREÇOS</h2>
                    <h3>ESCOLHA O PACOTE QUE MAIS ATENDE A SUA NECESSIDADE E ENTRE EM CONTATO</h3>
                    <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                        <div class='price'>
                            <h3><span>R$</span>800,00</h3>
                            <h4>LIGHT</h4>
                            <p class='l1'>
                                Onepage ou Paginado
                            </p>
                            <p class='l2'>
                                4 Paginas
                            </p>
                            <p class='l1'>
                                Layout Personalizado
                            </p>
                        </div>
                    </div>
                    <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                        <div class='price'>
                            <h3><span>R$</span>1000,00</h3>
                            <h4>LIGHT</h4>
                            <p class='l1'>
                                Onepage ou Paginado
                            </p>
                            <p class='l2'>
                                5 ~ 8 Paginas
                            </p>
                            <p class='l1'>
                                Layout Personalizado
                            </p>
                            <p class='l2'>
                                Banner Slider Basico(Imagem)
                            </p>
                        </div>
                    </div>
                    <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                        <div class='price'>
                            <h3><span>R$</span>1500,00</h3>
                            <h4>LIGHT</h4>
                            <p class='l1'>
                                Onepage ou Paginado
                            </p>
                            <p class='l2'>
                                9 ~ 12 Paginas
                            </p>
                            <p class='l1'>
                                Layout Personalizado
                            </p>
                            <p class='l2'>
                                Slider de banners Avançado(imagem + html)
                            </p>
                            <p class='l1'>
                                SEO para o site
                            </p>
                        </div>
                    </div>
                    <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
                        <div class='price'>
                            <h3><span>R$</span>2000,00</h3>
                            <h4>LIGHT</h4>
                            <p class='l1'>
                                Onepage ou Paginado
                            </p>
                            <p class='l2'>
                                9 ~ 12 Paginas
                            </p>
                            <p class='l1'>
                                Layout Personalizado
                            </p>
                            <p class='l2'>
                                Slider de banners Avançado(imagem + html)
                            </p>
                            <p class='l1'>
                                Responsivo
                            </p>
                            <p class='l2'>
                                Painel Administração (conteúdo)
                            </p>
                        </div>
                    </div>
                    <br/>
                    <p class='price-info'>
                        Caso haja o interesse em outros serviços oferecidos pela Ambient Arts, não exite em entrar em contato conosco. Temos certeza que faremos o melhor para atender as suas necessidades.
                    </p>
                </div>
            </div>
        </section>
        <section id='skill'>
            <div class='container'>
                <div class='row'>
                    <h2>NOSSOS PREÇOS</h2>
                    <h3>ESCOLHA O PACOTE QUE MAIS ATENDE A SUA NECESSIDADE E ENTRE EM CONTATO</h3>
                    <div class='graphs'>
                        <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6'>
                            <div class="second circle">
                                <span>
                                    HTML5/CSS3
                                </span>
                            </div>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6'>
                            <div class="second circle">
                                <span>
                                    JAVASCRIPT
                                </span>
                            </div>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6'>
                            <div class="second circle">
                                <span>
                                    ECOMMERCE
                                </span>
                            </div>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6'>
                            <div class="second circle">
                                <span>
                                    GOOGLE
                                </span>
                            </div>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6'>
                            <div class="second circle">
                                <span>
                                    BRANDING
                                </span>
                            </div>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6'>
                            <div class="second circle">
                                <span>
                                    MARKETING
                                </span>
                            </div>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6'>
                            <div class="second circle">
                                <span>
                                    PUBLICIDADE
                                </span>
                            </div>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6'>
                            <div class="second circle">
                                <span>
                                    MÍDIAS SOCIAIS
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class='port'>
                        <div class='col-lg-8 col-md-8 col-sm-12 col-xs-12'>
                            <h3>UM POUCO DO NOSSO TRABALHO</h3>
                            <h4>CLIQUI NO BOTÃO AO LADO E CONHEÇA ALGUNS DOS TRABALHOS QUE JÁ DESENVOLVEMOS</h4>
                        </div>
                        <div class='col-lg-4 col-md-4 col-sm-12 col-xs-12'>
                            <a href='#'>
                                <button type="button" class="btn btn-info">ACESSE NOSSO PORTFÓLIO</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id='contact'>
            <div class='container'>
                <div class='row'>
                    <h2>ENTRE EM CONTATO</h2>
                    <h3>JUNTOS PODEMOS DESENVOLVER O MELHOR PROJETO</h3>
                    <div class='form'>
                        <div class='col-lg-5 col-md-5 col-sm-6 col-xs-12'>
                            <div class="form-group">
                                <label for="nome">NOME</label>
                                <input type="text" class="form-control" id="nome" name='nome'>
                            </div>
                            <div class="form-group">
                                <label for="email">EMAIL</label>
                                <input type="text" class="form-control" id="email" name='email'>
                            </div>
                            <div class="form-group">
                                <label for="assunto">ASSUNTO</label>
                                <input type="text" class="form-control" id="assunto" name='assunto'>
                            </div>
                        </div>
                        <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'>
                            <div class="form-group">
                                <label for="mensagem">MENSAGEM:</label>
                                <textarea class="form-control" rows="5" id="mensagem" name='mensagem'></textarea>
                            </div>
                        </div>
                        <div class='col-lg-3 col-md-3 col-sm-12 col-xs-12'>
                            <p>
                                E-MAIL
                            </p>
                            <p>
                                CONTATO@AMBIEMTARTS.COM.BR
                            </p>
                            <BR/>
                            <P>
                                TELEFONE
                            </P>
                            <P>
                                (11) 96387-7371
                            </P>
                        </BR/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id='footer'>
            <div class='container'>
                <div class='row'>
                    <p>
                        TODOS OS DIREITOS RESERVADOS POR AMBIENT ARTST ©
                    </p>
                </div>
            </div>
        </section>
    </body>
</html>
