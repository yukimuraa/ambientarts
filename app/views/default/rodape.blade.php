        <footer class="page-footer  grey darken-4">
            <div class="container">
                <div class="row">
                    <div class="col s12 m4">
                        <img src="{{url('packages/images/logo-branco-sweet.png')}}" class="responsive-img" alt="Sweet Hair Professional">
                        <h5 class="white-text">Sweet Hair Professional</h5>
                        <ul>
                            <li class="grey-text text-lighten-4"><i class="fa fa-map-marker"></i>
                                 Rua Coelho Lisboa, 556 / São Paulo, SP
                            </li>
                            <li class="grey-text text-lighten-4">
                                <i class="fa fa-phone"></i> (11) 2227-8600<br /><i class="fa fa-phone"></i> (11) 4655-2130
                            </li>
                            <li class="grey-text text-lighten-4">
                                <i class="fa fa-envelope"></i> contato@sweethair.com.br
                            </li>
                        </ul>
                    </div>
                    <div class="col s12 m4">
                        <h5 class="white-text">Sobre</h5>
                        <p class="grey-text text-lighten-4">A Sweet Hair Professional é uma marca composta por um grupo de 7 empresas. Sua expertise está na constante inovação, que vai desde pesquisas de mercado e de novas matérias primas até o desenvolvimento de novos produtos que até então só eram imaginados em seus sonhos mais bonitos. <a href="http://sweethair.com.br/quem-somos" target="_blank">Mais...</a></p>
                    </div>
                    <div class="col s12 m4">
                        <div class="fb-page" data-href="https://www.facebook.com/produtodossonhos" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/produtodossonhos"><a href="https://www.facebook.com/produtodossonhos">Produto dos Sonhos</a></blockquote></div></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m4">
                        <h5 class="white-text">Menu</h5>
                        <ul>
                            <li><a class="grey-text" href="/termos">Política e Devolução</a></li>
                            <li><a class="grey-text" href="/loja">Linha Completa</a></li>
                            <li><a class="grey-text" href="http://produtodossonhos.com.br/infos.php">Entenda o Projeto</a></li>
                            <li><a class="grey-text" href="/blog">Blog</a></li>
                            <li><a class="grey-text" href="http://sweethair.com.br/" target="_blank">Sweet Hair</a></li>
                        </ul>
                    </div>
                    <div class="col s12 m4">
                        <h5 class="white-text">Categorias</h5>
                        <ul>
                            <li><a class="grey-text" href="{{url("/categoria/kit")}}">Kits</a></li>
                            <li><a class="grey-text" href="{{url("/categoria/shampoo")}}">Shampoo</a></li>
                            <li><a class="grey-text" href="{{url("/categoria/condicionador")}}">Condicionador</a></li>
                            <li><a class="grey-text" href="{{url("/categoria/mascara")}}">Máscara</a></li>
                            <li><a class="grey-text" href="{{url("/categoria/finalizadores")}}">Finalizadores</a></li>
                            <li><a class="grey-text" href="{{url("/categoria/tratamento_capilar")}}">Tratamento Capilar</a></li>
                            <li><a class="grey-text" href="{{url("embaixadora/andreza_goulart")}}">Andreza Goulart</a></li>
                            <li><a class="grey-text" href="{{url("embaixadora/fabi_santina")}}">Fabi Santina</a></li>
                            <li><a class="grey-text" href="{{url("embaixadora/kathy_castricini")}}">Kathy Castricini</a></li>
                            <li><a class="grey-text" href="{{url("embaixadora/laila_coelho")}}">Laila Coelho</a></li>
                            <li><a class="grey-text" href="{{url("embaixadora/raquel_minelli")}}">Raquel Minelli</a></li>
                            <li><a class="grey-text" href="{{url("embaixadora/vanessa_wonsovicz")}}">Vanessa Wonsovicz</a></li>
                        </ul>
                    </div>
                    <div class="col s12 m4">
                        <a href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/index.html#url=produtodossonhos.com.br/loja/" target="_blank"><img src="{{url('packages/images/safe_google_browsing.png')}}" class="responsive-img" alt="Google Safe Browsing" style="display:block;"></a>
                        <div class="espaco30"></div>
                        <img src="{{url('packages/images/site_seguro.png')}}" class="responsive-img" alt="Site Seguro" style="display:block;">
                        <div class="espaco30"></div>
                        <img src="{{url('packages/images/formas-de-pagamento_small.png')}}" class="responsive-img" alt="Formas de Pagamento" style="display:block;">
                    </div>
                </div>
            </div>
            <div class="footer-copyright black">
                <div class="container">2015 - Sweet Hair Professional. Todos os direitos são reservados.
                    <a class="grey-text text-lighten-4 right" href="http://sevenlink.com.br/" target="_blank">Design Agência Seven Link</a>
                </div>
            </div>
        </footer>
        {{HTML::script("packages/js/jquery.nicescroll.min.js")}}
        {{HTML::script("packages/js/jquery.price_format.2.0.min.js")}}
        {{HTML::script("packages/js/jquery.mask.js")}}
        {{HTML::script("packages/js/jquery.validate.js")}}
        {{HTML::script("packages/js/global.js")}}
        {{HTML::script("packages/js/cart.js")}}

        <div id="modal_comprar" class="modal">
            <div class="modal-content">
                <h5>CONDICIONAR RECONTRUÇÃO 260ML </h5>
                <p>foi adicionado com sucesso ao seu carrinho de compras.</p>
                <div class="modal-footer">
                    <div class="col s12 m6">
                        <a href="#!" class="modal-action modal-close waves-effect waves-light btn teal lighten-3 left">Continuar comprando</a>
                    </div>
                    <div class="col s12 m6">
                        <a href="/pedido/confirma" class="waves-effect waves-light btn pink accent-2 right">Finalizar compra</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
