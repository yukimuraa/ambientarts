{{HTML::style("packages/css/slider.css")}}

<script type="text/javascript">
    $(document).ready(function(){
        $('.slider').slider({full_width: true, height: 500});
    });
</script>
<div class="row">
    <div class="slider">
        <ul class="slides">
            <?php
                $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
                $ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
                $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
                $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
                $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
                $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
                $symbian =  strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");
            ?>
            @if(($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true))
                <li>
                    <img src="{{url("packages/images/banner/andreza_goulart_mobile.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/fabi_santina_mobile.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/kathy_castricini_mobile.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/laila_coelho_mobile.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/raka_minelli_mobile.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/vanessa_wonsovicz_mobile.jpg")}}">
                </li>
            @else
                {{-- <li>
                    <img src="{{url("packages/images/banner/banner_black_friday.jpg")}}">
                </li> --}}
                <li>
                    <img src="{{url("packages/images/banner/andreza_goulart.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/fabi_santina.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/kathy_castricini.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/laila_coelho.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/raka_minelli.jpg")}}">
                </li>
                <li>
                    <img src="{{url("packages/images/banner/vanessa_wonsovicz.jpg")}}">
                </li>
            @endif
        </ul>
    </div>
</div>
