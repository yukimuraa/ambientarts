<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    @if(isset($titulo))
        <title>Produto dos Sonhos | {{$titulo}}</title>
    @else
        <title>Produto dos Sonhos</title>
    @endif
    <!--meta SEO-->
    <meta name="robots" content="index, follow">
    @if(isset($produto))
        <meta property="og:description"   content="{{$produto->produto_descricao}}" />
    @else
        <meta name="og:description" content="Cosméticos capilares Sweet Hair feito com as blogueiras famosas de todo o Brasil">
    @endif
    <meta name="Keywords" content="Sweet Hair, Produto dos Sonhos, Cosméticos, Blogueiras, Blog, Fabi Santina, Andreza Goulart, Raquel Minelli, Vanessa Wonsovicz, Laila Coelho, Kathy Castricini, shampoo, condicionador, tratamento, cabelo lindo, produtos para cabelo, tratamento capilar, cbbllgers">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{url('packages/font/font-awesome/css/font-awesome.min.css')}}">
    <!--Import materialize.css-->
    {{HTML::style("packages/css/materialize.css")}}
    {{HTML::style("packages/css/style.css")}}
    {{HTML::style("packages/css/header.css")}}
    {{HTML::style("packages/css/cart.css")}}
    {{HTML::style("packages/css/media-hover-effects.css")}}
    <!-- {{HTML::style("packages/css/icon.css")}} -->

    <!--Icones para todos os dispositivos-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{url('packages/images/favicons/apple-touch-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{url('packages/images/favicons/apple-touch-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{url('packages/images/favicons/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('packages/images/favicons/apple-touch-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{url('packages/images/favicons/apple-touch-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{url('packages/images/favicons/apple-touch-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{url('packages/images/favicons/apple-touch-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url('packages/images/favicons/apple-touch-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('packages/images/favicons/apple-touch-icon-180x180.png')}}">
    <link rel="icon" type="image/png" href="{{url('packages/images/favicons/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{url('packages/images/favicons/android-chrome-192x192.png')}}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{url('packages/images/favicons/favicon-96x96.png')}}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{url('packages/images/favicons/favicon-16x16.png')}}" sizes="16x16">
    <link rel="manifest" href="{{url('packages/manifest.json')}}">
    <link rel="mask-icon" href="{{url('packages/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <!--Import jQuery before materialize.js-->
    {{HTML::script("packages/js/jquery-2.1.4.min.js")}}
    {{HTML::script("packages/js/materialize.js")}}
    <script type="text/javascript">
        $(document).ready(function(){
            $(".button-collapse").sideNav();
            $('.modal-trigger').leanModal();
            $('.parallax').parallax();
        });
    </script>
    <script src="https://apis.google.com/js/platform.js" async defer>
      {lang: 'pt-BR'}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-46721856-12', 'auto');
        ga('require', 'linkid');
        ga('send', 'pageview');
    </script>
</head>
@include('default/menu')
@include('visitante/login')
