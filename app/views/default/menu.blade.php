<body>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=1628892563991794";
            fjs.parentNode.insertBefore(js, fjs);
        }
        (document, 'script', 'facebook-jssdk'));
    </script>
    <header>
        <section id="cabecalho">
            <div class="navbar-fixed ">
                <nav class="bg_verde white-text header-top" >
                    <div class='container row'>
                        <div class="col s6 hide-on-med-and-down">
                            <a href=''><span>contato:</span>(11) 2227-8600</a>
                        </div>
                        <div class="col l6 m12 s12">
                            <a href="#" data-activates="slide-out" class="menu-sidebar-collapse btn-floating btn-flat btn-medium waves-effect waves-light cyan hide-on-large-only"><i class="mdi-navigation-menu"></i></a>
                            <ul class='en-cart right'>
                                @if(Auth::user()->check())
                                    <li><a href="{{url('usuario')}}" class='white-text'>MINHA CONTA</a></li>
                                    <li><a href="{{url('sair')}}" class='white-text'>SAIR </a></li>
                                @else
                                    <li><a href="#" class="cd-login">LOGIN</a></li>
                                    <li><a href="#" class="cd-signin">CADASTRAR </a></li>
                                @endif
                                <li >
                                    <a href='#' data-activates="cd-cart" class='text-cart tougle-cart'><i class="medium material-icons">shopping_basket</i> <span>R$ 0,00</span></a>
                                </li>
                                <li >
                                    <a href='#' data-activates="cd-search" class='tougle-search'><i class="fa fa-search"></i></a>
                                    <div class='form-search col l3 m5 s12'>
                                        <form method="GET" action='/busca' class='frm-sc'>
                                            <button type='button' class='tougle-search btn-scCancel  col l2 s2 '><i class="fa fa-times"></i></button>
                                            <input type='text' class='inp-sc col l8 s8' placeholder="Pesquisar..." name='sc'>
                                            <button type='submit' class='btn-sc  col l2 s2'><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="header-bg">
                <div class="container center nav-log" style="padding:10px;">
                    <a href="/home">
                        <img id="logo" src="{{url('packages/images/logo_produto_sonhos_320.png')}}" class="responsive-img hide-on-small-only" width="280" alt="Produto dos Sonhos">
                        <!--mobile-->
                        <img id="logo" src="{{url('packages/images/logo_produto_sonhos_140.png')}}" class="responsive-img hide-on-med-and-up" width="130" alt="Produto dos Sonhos">
                    </a>
                </div>
            </div>
            <nav class="grey lighten-4 hide-on-med-and-down" id='menu-fixed' >
                <div class="nav-wrapper container">
                    <ul class="center menu-center">
                        <li><a href="{{url('/home')}}">Home</a></li>
                        <li><a href="{{url('/loja')}}">Loja</a></li>
                        <li><a class="dropdown-button" href="#!" data-activates="drop_emb">Embaixadoras<i class="mdi-navigation-arrow-drop-down right"></i></a></li>
                        <ul id='drop_emb' class='dropdown-content'>
                            <li><a href="{{url("embaixadoras/andreza_goulart")}}">Andreza Goulart</a></li>
                            <li><a href="{{url("embaixadoras/fabi_santina")}}">Fabi Santina</a></li>
                            <li><a href="{{url("embaixadoras/kathy_castricini")}}">Kathy Castricini</a></li>
                            <li><a href="{{url("embaixadoras/laila_coelho")}}">Laila Coelho</a></li>
                            <li><a href="{{url("embaixadoras/raka_minelli")}}">Raka Minelli</a></li>
                            <li><a href="{{url("embaixadoras/vanessa_wonsovicz")}}">Vanessa Wonsovicz</a></li>
                        </ul>
                        <li><a class="dropdown-button" href="#!" data-activates="drop_cat">Categorias<i class="mdi-navigation-arrow-drop-down right"></i></a></li>
                        <ul id='drop_cat' class='dropdown-content'>
                            <li><a href="{{url("categorias/kit")}}">Kits</a></li>
                            <li><a href="{{url("categorias/shampoo")}}">Shampoo</a></li>
                            <li><a href="{{url("categorias/condicionador")}}">Condicionador</a></li>
                            <li><a href="{{url("categorias/mascara")}}">Máscara</a></li>
                            <li><a href="{{url("categorias/finalizadores")}}">Finalizadores</a></li>
                            <li><a href="{{url("categorias/tratamento_capilar")}}">Tratamento Capilar</a></li>
                        </ul>
                        <li><a href="{{url('/blog')}}">Blog</a></li>
                        <li><a href="http://produtodossonhos.com.br/infos.php">Entenda o Projeto</a></li>
                    </ul>
                </div>
            </nav>
            <ul id="slide-out" class="side-nav leftside-navigation ps-container ps-active-y" >
                <li class="bold"><a href="{{url('/home')}}">Home</a></li>
                <li class="bold"><a href="{{url('/loja')}}">Loja</a></li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold">
                            <a class="collapsible-header">Embaixadoras</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="{{url("embaixadoras/andreza_goulart")}}">Andreza Goulart</a></li>
                                    <li><a href="{{url("embaixadoras/fabi_santina")}}">Fabi Santina</a></li>
                                    <li><a href="{{url("embaixadoras/kathy_castricini")}}">Kathy Castricini</a></li>
                                    <li><a href="{{url("embaixadoras/laila_coelho")}}">Laila Coelho</a></li>
                                    <li><a href="{{url("embaixadoras/raka_minelli")}}">Raka Minelli</a></li>
                                    <li><a href="{{url("embaixadoras/vanessa_wonsovicz")}}">Vanessa Wonsovicz</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold">
                            <a class="collapsible-header">Categorias</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="{{url("categorias/kit")}}">Kits</a></li>
                                    <li><a href="{{url("categorias/shampoo")}}">Shampoo</a></li>
                                    <li><a href="{{url("categorias/condicionador")}}">Condicionador</a></li>
                                    <li><a href="{{url("categorias/mascara")}}">Máscara</a></li>
                                    <li><a href="{{url("categorias/finalizadores")}}">Finalizadores</a></li>
                                    <li><a href="{{url("categorias/tratamento_capilar")}}">Tratamento Capilar</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="http://produtodossonhos.com.br/infos.php">Entenda o Projeto</a></li>
                </li>
            </ul>
            <div id="cd-cart" >
                <h2>Minha Bolsa</h2>
                <ul class="cd-cart-items"></ul> <!-- cd-cart-items -->

                <div class="cd-cart-total">
                    <p>Total <span>R$ 0,00</span></p>
                </div> <!-- cd-cart-total -->

                <a href="{{url('/pedido/confirma')}}" class="checkout-btn pink accent-2">Finalizar</a>
                <br>
                <a href="{{url('/bolsa')}}" class="cart-btn teal lighten-3">Ir para Bolsa</a>
            </div>
        </section>
    </header>
