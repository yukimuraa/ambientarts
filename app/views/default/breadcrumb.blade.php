<div class="row">
    <div class="espaco20"></div>
    <div class="col s12 m6 breadcrumb">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                    <a href="{{url('home')}}">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            @for($i = 0; $i <= count(Request::segments()); $i++)
                @if(Request::segment($i) != '')
                    <li>
                        <a href="{{url(Request::segment($i))}}">{{ucfirst(Request::segment($i))}}</a>
                        @if($i < count(Request::segments()) & $i > 0)
                            <i class="fa fa-angle-right"></i>
                        @endif
                    </li>
                @endif
            @endfor
        </ul>
    </div>
    <div class="col s12 m6 right breadcrumb hide-on-small-only">
        <span class="right"><a href="javascript: history.go(-1)"><i class="fa fa-angle-left"></i> Página anterior</span></a>
    </div>
</div>
<div class="divider"></div>
